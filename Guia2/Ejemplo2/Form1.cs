﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo2
{
    public partial class Ejemplo2 : Form
    {
        public Ejemplo2()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //agrega item al combo
            cmbop.Items.Clear();
            cmbop.Items.Add("Sumar");
            cmbop.Items.Add("Restar");

            //agrega item a la lista
            listAdvance.Items.Clear();
            listAdvance.Items.Add("Multiplicación");
            listAdvance.Items.Add("División");
            listAdvance.TabIndex = 0;

            //inhabilita el combo y la lista
            //cmbop.Enabled = false;
            listAdvance.Enabled = false;
        }

        private void rbtn1_CheckedChanged(object sender, EventArgs e)
        {
            if(rbtn1.Checked == true)
            {
                cmbop.Enabled = true;
                listAdvance.Enabled = false;
            }
        }

        private void rbtn2_CheckedChanged(object sender, EventArgs e)
        {
            cmbop.Enabled = false;
            listAdvance.Enabled = true;

            //me pérmite seleccionar el primer elemento de la lista
            listAdvance.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double n1, n2, r;

            n1 = Convert.ToDouble(txtnum1.Text);
            n2 = Convert.ToDouble(txtnum2.Text);

            if(cmbop.Enabled == true)
            {
                if(cmbop.SelectedItem.ToString() == "Sumar")
                {
                    r = n1 + n2;
                }
                else
                {
                    r = n1 - n2;
                }
                MessageBox.Show("El resultado es " + r.ToString(), "Respuesta");
            }
            
            

            if (listAdvance.Enabled == true)
            {
                if (listAdvance.SelectedItem.ToString() == "Multiplicación")
                {
                    r = n1 * n2;
                }
                else
                {
                    r = n1 / n2;
                }
                MessageBox.Show("El resultado es " + r.ToString(), "Respuesta");
            }
            
            
        }
    }
}
