﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejemplo_04
{
    public partial class G1_Ejemplo_04 : Form
    {
        string password;
        public G1_Ejemplo_04()
        {
            InitializeComponent();
            
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text; //capturamos los valoes de usuario y contraseña
            string contra = txtContra.Text;

            string url = "..\\" + usuario + ".txt";
            if (File.Exists(url)) //verifica si existe
            {
                password = File.ReadAllText(url); //lee el texto almacenado dentro del archivo
                if (contra.Equals(password)) // verifica si contraseña es igual al archivo 
                {
                    MessageBox.Show("¡Ingreso exitoso, bienvenido!"); //login exitoso
                }
                else
                {
                    MessageBox.Show("¡Contraseña incorrecta!"); //login fallido
                }
            }
            else
            {
                MessageBox.Show("¡Usuario incorrecto!"); //usuario incorrecto
            }
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text;
            string contra = txtContra.Text;
            string url = "..\\" + usuario + ".txt";
            //..\Publications\TravelBrochure.pdf
            if (File.Exists(url))  //verifica que el archivo existe;
            {
                MessageBox.Show("ERROR. ¡Usuario ya existe!"); //usuario registrado
                txtUsuario.Clear();
                txtContra.Clear();
            }
            else
            {
                File.WriteAllText(url, contra); //Crea un nuevo archivo con ese nomnbre y guarda dentro del archivo el valor del segundo párametro
                MessageBox.Show("Usuario registrado con éxito");
                txtUsuario.Clear();
                txtContra.Clear();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
