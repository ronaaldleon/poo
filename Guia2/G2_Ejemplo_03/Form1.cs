﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejemplo_03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string texto = txtNombre.Text;
            string mensaje = string.Format("Bienvenido al segundo formulario " + texto);
            frmRecibe frmRecibe = new frmRecibe (mensaje);//creo un objeto del segundo formulario, adonde mando información
            frmRecibe.Visible = true; //muestra el nuevo formulario
            this.Visible = false; //esconde el formulario actual
            //MessageBox.Show("Bienvenido a POO " + nombre + " este es tu primer formulario");
        }

        private void btnSalir_Click_1(object sender, EventArgs e)
        {
            Application.Exit(); //termina la aplicación
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
