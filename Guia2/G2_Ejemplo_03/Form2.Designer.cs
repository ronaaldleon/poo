﻿namespace G2_Ejemplo_03
{
    partial class frmRecibe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbRecibido = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbRecibido
            // 
            this.lbRecibido.AutoSize = true;
            this.lbRecibido.Location = new System.Drawing.Point(22, 28);
            this.lbRecibido.Name = "lbRecibido";
            this.lbRecibido.Size = new System.Drawing.Size(35, 13);
            this.lbRecibido.TabIndex = 0;
            this.lbRecibido.Text = "label1";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Location = new System.Drawing.Point(234, 109);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(75, 23);
            this.btnRegresar.TabIndex = 1;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // frmRecibe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 142);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.lbRecibido);
            this.Name = "frmRecibe";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.frmRecibe_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbRecibido;
        private System.Windows.Forms.Button btnRegresar;
    }
}