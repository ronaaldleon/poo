﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejemplo_03
{
    public partial class frmRecibe : Form
    {
        public frmRecibe()
        {
            InitializeComponent();
        }

        private void frmRecibe_Load(object sender, EventArgs e)
        {
            
        }

        public frmRecibe(String textx)
        {
            InitializeComponent();
            lbRecibido.Text = textx; //asignamos lo recibido al label
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();//instanciamos al primer formulario
            this.Close(); //cerramos el formulario actual
            form1.Visible = true; //hacemos visible el form1 de nuevo
        }
    }
}
