﻿
namespace G4_Ejemplo_05
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.listArreglo = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCalc1 = new System.Windows.Forms.TextBox();
            this.txtCalc2 = new System.Windows.Forms.TextBox();
            this.txtCalc3 = new System.Windows.Forms.TextBox();
            this.txtCalc4 = new System.Windows.Forms.TextBox();
            this.btnCalc1 = new System.Windows.Forms.Button();
            this.btnCalc2 = new System.Windows.Forms.Button();
            this.btnCalc3 = new System.Windows.Forms.Button();
            this.btnCalc4 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listArreglo
            // 
            this.listArreglo.ForeColor = System.Drawing.Color.Black;
            this.listArreglo.FormattingEnabled = true;
            this.listArreglo.Location = new System.Drawing.Point(12, 78);
            this.listArreglo.Name = "listArreglo";
            this.listArreglo.Size = new System.Drawing.Size(151, 212);
            this.listArreglo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ingrese un valor al arreglo";
            // 
            // txtNumero
            // 
            this.txtNumero.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumero.Location = new System.Drawing.Point(188, 17);
            this.txtNumero.Multiline = true;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(100, 35);
            this.txtNumero.TabIndex = 2;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(313, 19);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(75, 23);
            this.btnIngresar.TabIndex = 3;
            this.btnIngresar.Text = "INGRESAR";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Número mayor de pares negativos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Porcentaje de ceros en arreglo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Promedio de impares positivos";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCalc4);
            this.groupBox1.Controls.Add(this.btnCalc3);
            this.groupBox1.Controls.Add(this.btnCalc2);
            this.groupBox1.Controls.Add(this.btnCalc1);
            this.groupBox1.Controls.Add(this.txtCalc4);
            this.groupBox1.Controls.Add(this.txtCalc3);
            this.groupBox1.Controls.Add(this.txtCalc2);
            this.groupBox1.Controls.Add(this.txtCalc1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(186, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 215);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OPERACIONES CON ARREGLOS";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Mayor de los pares positivos";
            // 
            // txtCalc1
            // 
            this.txtCalc1.Location = new System.Drawing.Point(191, 26);
            this.txtCalc1.Name = "txtCalc1";
            this.txtCalc1.Size = new System.Drawing.Size(100, 20);
            this.txtCalc1.TabIndex = 8;
            // 
            // txtCalc2
            // 
            this.txtCalc2.Location = new System.Drawing.Point(191, 69);
            this.txtCalc2.Name = "txtCalc2";
            this.txtCalc2.Size = new System.Drawing.Size(100, 20);
            this.txtCalc2.TabIndex = 9;
            // 
            // txtCalc3
            // 
            this.txtCalc3.Location = new System.Drawing.Point(191, 122);
            this.txtCalc3.Name = "txtCalc3";
            this.txtCalc3.Size = new System.Drawing.Size(100, 20);
            this.txtCalc3.TabIndex = 10;
            // 
            // txtCalc4
            // 
            this.txtCalc4.Location = new System.Drawing.Point(191, 176);
            this.txtCalc4.Name = "txtCalc4";
            this.txtCalc4.Size = new System.Drawing.Size(100, 20);
            this.txtCalc4.TabIndex = 11;
            // 
            // btnCalc1
            // 
            this.btnCalc1.Location = new System.Drawing.Point(313, 24);
            this.btnCalc1.Name = "btnCalc1";
            this.btnCalc1.Size = new System.Drawing.Size(75, 23);
            this.btnCalc1.TabIndex = 12;
            this.btnCalc1.Text = "MOSTRAR";
            this.btnCalc1.UseVisualStyleBackColor = true;
            this.btnCalc1.Click += new System.EventHandler(this.btnCalc1_Click);
            // 
            // btnCalc2
            // 
            this.btnCalc2.Location = new System.Drawing.Point(313, 66);
            this.btnCalc2.Name = "btnCalc2";
            this.btnCalc2.Size = new System.Drawing.Size(75, 23);
            this.btnCalc2.TabIndex = 13;
            this.btnCalc2.Text = "MOSTRAR";
            this.btnCalc2.UseVisualStyleBackColor = true;
            this.btnCalc2.Click += new System.EventHandler(this.btnCalc2_Click);
            // 
            // btnCalc3
            // 
            this.btnCalc3.Location = new System.Drawing.Point(313, 124);
            this.btnCalc3.Name = "btnCalc3";
            this.btnCalc3.Size = new System.Drawing.Size(75, 23);
            this.btnCalc3.TabIndex = 14;
            this.btnCalc3.Text = "MOSTRAR";
            this.btnCalc3.UseVisualStyleBackColor = true;
            this.btnCalc3.Click += new System.EventHandler(this.btnCalc3_Click);
            // 
            // btnCalc4
            // 
            this.btnCalc4.Location = new System.Drawing.Point(313, 176);
            this.btnCalc4.Name = "btnCalc4";
            this.btnCalc4.Size = new System.Drawing.Size(75, 23);
            this.btnCalc4.TabIndex = 15;
            this.btnCalc4.Text = "MOSTRAR";
            this.btnCalc4.UseVisualStyleBackColor = true;
            this.btnCalc4.Click += new System.EventHandler(this.btnCalc4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 327);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listArreglo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listArreglo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalc4;
        private System.Windows.Forms.Button btnCalc3;
        private System.Windows.Forms.Button btnCalc2;
        private System.Windows.Forms.Button btnCalc1;
        private System.Windows.Forms.TextBox txtCalc4;
        private System.Windows.Forms.TextBox txtCalc3;
        private System.Windows.Forms.TextBox txtCalc2;
        private System.Windows.Forms.TextBox txtCalc1;
        private System.Windows.Forms.Label label5;
    }
}

