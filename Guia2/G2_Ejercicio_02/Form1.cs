﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejercicio_02
{
    public partial class frmConversiones : Form
    {
        public frmConversiones()
        {
            InitializeComponent();
        }

        private void brnCalcular_Click(object sender, EventArgs e)
        {
            string dato1;
            double num1, num2;

            dato1 = txtUno.Text;

            //validación que el campo no este vacío
            if (dato1 != "")
            {
                //cadena de expresiones regulares donde solo se aceptan números
                Regex regex = new Regex(@"^[0-9]+$");

                //validación si el campo de txtBruto contiene solo número comparando el campo con la expresión regular
                if (regex.IsMatch(dato1))
                {
                    //conversión de string a double
                    num1 = double.Parse(dato1);

                    //si el radiobutton ha sido seleccinado...
                    if(rbtnTemp.Checked == true)
                    {
                        num2 = (num1 * 9/5) + 32;
                        txtDos.Text = num2.ToString();
                        lblAviso.ResetText();
                    }
                    else if (rbtnLong.Checked == true)
                    {
                        num2 = num1 * 3.281;
                        txtDos.Text = num2.ToString();
                        lblAviso.ResetText();
                    }
                    else if (rbtnPeso.Checked == true)
                    {
                        num2 = num1 * 2.2;
                        txtDos.Text = num2.ToString();
                        lblAviso.ResetText();
                    }
                    else
                    {
                        lblAviso.Text = "¡Seleccione el tipo de conversión!";
                    }
                }
                else
                {
                    lblAviso.Text = "¡Solo se permiten números!";
                }
            }
            else
            {
                lblAviso.Text = "¡Rellene el campo vacío!";
            }
        }

        private void rbtnTemp_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnTemp.Checked == true)
            {
                txtUno.Clear();
                txtDos.Clear();
                lblUno.Text = "° Celcius";
                lblDos.Text = "° Farenheit";
            }
        }

        private void rbtnLong_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnLong.Checked == true)
            {
                txtUno.Clear();
                txtDos.Clear();
                lblUno.Text = "Metros";
                lblDos.Text = "Pies";
            }
        }

        private void rbtnPeso_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnPeso.Checked == true)
            {
                txtUno.Clear();
                txtDos.Clear();
                lblUno.Text = "Kilogramos";
                lblDos.Text = "Libras";
            }
        }
    }
}
