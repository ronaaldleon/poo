﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    class Individuo
    {
        protected string  nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        protected string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        protected string codigo;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

    }

    class Docente : Individuo //herencia de la clase Individuo
    {
        protected string materia;
        public string Materia
        {
            get { return materia; }
            set { materia = value; }
        }
    }
}
