﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    class Persona
    {
        protected string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        protected string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
    }
    class Empleado : Persona
    {

    }
    class Estudiante : Persona
    {
        protected string carnet;

        public string Carnet
        {
            get { return carnet; }
            set { carnet = value; }
        }

        protected string nivelEstudio;
        public string NivelEstudio
        {
            get { return nivelEstudio; }
            set { nivelEstudio = value; }
        }

    }
    class Universitario : Estudiante
    {
        protected string nombreUni;
        public string NombreUni
        {
            get { return nombreUni; }
            set { nombreUni = value; }
        }

        protected string carrera;
        public string Carrera
        {
            get { return carrera; }
            set { carrera = value; }
        }

        protected string materiasInscritas;
        public string MateriasInscritas
        {
            get { return materiasInscritas; }
            set { materiasInscritas = value; }
        }

        protected string notas;
        public string Notas
        {
            get { return notas; }
            set { notas = value; }
        }

        protected string cum;
        public string CUM
        {
            get { return cum; }
            set { cum = value; }
        }
    }
    class Ingenieria : Universitario
    {
        protected string nombreProyecto;
        public string NombreProyecto
        {
            get { return nombreProyecto; }
            set { nombreProyecto = value; }
        }

        protected string totalHoras;
        public string TotalHoras
        {
            get { return totalHoras; }
            set { totalHoras = value; }
        }

        protected string horasCompletas;
        public string HorasCompletas
        {
            get { return horasCompletas; }
            set { horasCompletas = value; }
        }
    }
}
