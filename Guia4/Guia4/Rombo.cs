﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;//agregamos esta libreria

namespace Guia4
{
    public class Rombo:Figura
    {
        //Diagonal Mayor
        private double diagoMayor;
        //Diagonal Menor
        private double diagoMenor;

        public Rombo(double A, double MA, double ME) : base(A)//Constructor
        {
            diagoMayor = MA;
            diagoMenor = ME;
        }

        public double DiagoMayor
        {
            get { return diagoMayor; }
            set { diagoMayor = value; }
        }

        public double DiagoMenor
        {
            get { return diagoMenor; }
            set { diagoMenor = value; }
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (DiagoMayor * DiagoMenor)/2;
            LR.Text = "Area: " + Area;
        }
    }
}
