﻿
namespace Guia5
{
    partial class Notas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.nudNota3P1 = new System.Windows.Forms.NumericUpDown();
            this.nudNota2P1 = new System.Windows.Forms.NumericUpDown();
            this.nudNota1P1 = new System.Windows.Forms.NumericUpDown();
            this.btnSiguienteP1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.nudNota3P2 = new System.Windows.Forms.NumericUpDown();
            this.nudNota2P2 = new System.Windows.Forms.NumericUpDown();
            this.nudNota1P2 = new System.Windows.Forms.NumericUpDown();
            this.btnSiguienteP2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.nudNota3P3 = new System.Windows.Forms.NumericUpDown();
            this.nudNota2P3 = new System.Windows.Forms.NumericUpDown();
            this.nudNota1P3 = new System.Windows.Forms.NumericUpDown();
            this.btnSiguienteP3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.MenuControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P3)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuControl
            // 
            this.MenuControl.Controls.Add(this.tabPage1);
            this.MenuControl.Controls.Add(this.tabPage2);
            this.MenuControl.Controls.Add(this.tabPage3);
            this.MenuControl.Location = new System.Drawing.Point(12, 12);
            this.MenuControl.Name = "MenuControl";
            this.MenuControl.SelectedIndex = 0;
            this.MenuControl.Size = new System.Drawing.Size(261, 258);
            this.MenuControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.nudNota3P1);
            this.tabPage1.Controls.Add(this.nudNota2P1);
            this.tabPage1.Controls.Add(this.nudNota1P1);
            this.tabPage1.Controls.Add(this.btnSiguienteP1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(253, 229);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Periodo 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // nudNota3P1
            // 
            this.nudNota3P1.DecimalPlaces = 1;
            this.nudNota3P1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota3P1.Location = new System.Drawing.Point(106, 126);
            this.nudNota3P1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota3P1.Name = "nudNota3P1";
            this.nudNota3P1.Size = new System.Drawing.Size(100, 22);
            this.nudNota3P1.TabIndex = 9;
            this.nudNota3P1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota2P1
            // 
            this.nudNota2P1.DecimalPlaces = 1;
            this.nudNota2P1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota2P1.Location = new System.Drawing.Point(106, 74);
            this.nudNota2P1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota2P1.Name = "nudNota2P1";
            this.nudNota2P1.Size = new System.Drawing.Size(100, 22);
            this.nudNota2P1.TabIndex = 8;
            this.nudNota2P1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota1P1
            // 
            this.nudNota1P1.DecimalPlaces = 1;
            this.nudNota1P1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota1P1.Location = new System.Drawing.Point(106, 24);
            this.nudNota1P1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota1P1.Name = "nudNota1P1";
            this.nudNota1P1.Size = new System.Drawing.Size(100, 22);
            this.nudNota1P1.TabIndex = 7;
            this.nudNota1P1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSiguienteP1
            // 
            this.btnSiguienteP1.Location = new System.Drawing.Point(83, 176);
            this.btnSiguienteP1.Name = "btnSiguienteP1";
            this.btnSiguienteP1.Size = new System.Drawing.Size(84, 35);
            this.btnSiguienteP1.TabIndex = 6;
            this.btnSiguienteP1.Text = "Siguiente";
            this.btnSiguienteP1.UseVisualStyleBackColor = true;
            this.btnSiguienteP1.Click += new System.EventHandler(this.btnSiguienteP1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nota3:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nota2:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nota1:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.nudNota3P2);
            this.tabPage2.Controls.Add(this.nudNota2P2);
            this.tabPage2.Controls.Add(this.nudNota1P2);
            this.tabPage2.Controls.Add(this.btnSiguienteP2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(253, 229);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Periodo 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // nudNota3P2
            // 
            this.nudNota3P2.DecimalPlaces = 1;
            this.nudNota3P2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota3P2.Location = new System.Drawing.Point(106, 126);
            this.nudNota3P2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota3P2.Name = "nudNota3P2";
            this.nudNota3P2.Size = new System.Drawing.Size(100, 22);
            this.nudNota3P2.TabIndex = 15;
            this.nudNota3P2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota2P2
            // 
            this.nudNota2P2.DecimalPlaces = 1;
            this.nudNota2P2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota2P2.Location = new System.Drawing.Point(106, 74);
            this.nudNota2P2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota2P2.Name = "nudNota2P2";
            this.nudNota2P2.Size = new System.Drawing.Size(100, 22);
            this.nudNota2P2.TabIndex = 14;
            this.nudNota2P2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota1P2
            // 
            this.nudNota1P2.DecimalPlaces = 1;
            this.nudNota1P2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota1P2.Location = new System.Drawing.Point(106, 24);
            this.nudNota1P2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota1P2.Name = "nudNota1P2";
            this.nudNota1P2.Size = new System.Drawing.Size(100, 22);
            this.nudNota1P2.TabIndex = 13;
            this.nudNota1P2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSiguienteP2
            // 
            this.btnSiguienteP2.Location = new System.Drawing.Point(83, 176);
            this.btnSiguienteP2.Name = "btnSiguienteP2";
            this.btnSiguienteP2.Size = new System.Drawing.Size(84, 35);
            this.btnSiguienteP2.TabIndex = 12;
            this.btnSiguienteP2.Text = "Siguiente";
            this.btnSiguienteP2.UseVisualStyleBackColor = true;
            this.btnSiguienteP2.Click += new System.EventHandler(this.btnSiguienteP2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nota3:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Nota2:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Nota1:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nudNota3P3);
            this.tabPage3.Controls.Add(this.nudNota2P3);
            this.tabPage3.Controls.Add(this.nudNota1P3);
            this.tabPage3.Controls.Add(this.btnSiguienteP3);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(253, 229);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Periodo 3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // nudNota3P3
            // 
            this.nudNota3P3.DecimalPlaces = 1;
            this.nudNota3P3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota3P3.Location = new System.Drawing.Point(106, 126);
            this.nudNota3P3.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota3P3.Name = "nudNota3P3";
            this.nudNota3P3.Size = new System.Drawing.Size(100, 22);
            this.nudNota3P3.TabIndex = 18;
            this.nudNota3P3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota2P3
            // 
            this.nudNota2P3.DecimalPlaces = 1;
            this.nudNota2P3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota2P3.Location = new System.Drawing.Point(106, 74);
            this.nudNota2P3.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota2P3.Name = "nudNota2P3";
            this.nudNota2P3.Size = new System.Drawing.Size(100, 22);
            this.nudNota2P3.TabIndex = 17;
            this.nudNota2P3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota1P3
            // 
            this.nudNota1P3.DecimalPlaces = 1;
            this.nudNota1P3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota1P3.Location = new System.Drawing.Point(106, 24);
            this.nudNota1P3.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota1P3.Name = "nudNota1P3";
            this.nudNota1P3.Size = new System.Drawing.Size(100, 22);
            this.nudNota1P3.TabIndex = 16;
            this.nudNota1P3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSiguienteP3
            // 
            this.btnSiguienteP3.Location = new System.Drawing.Point(83, 176);
            this.btnSiguienteP3.Name = "btnSiguienteP3";
            this.btnSiguienteP3.Size = new System.Drawing.Size(84, 35);
            this.btnSiguienteP3.TabIndex = 12;
            this.btnSiguienteP3.Text = "Terminar";
            this.btnSiguienteP3.UseVisualStyleBackColor = true;
            this.btnSiguienteP3.Click += new System.EventHandler(this.btnSiguienteP3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Nota3:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nota2:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Nota1:";
            // 
            // Notas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 294);
            this.Controls.Add(this.MenuControl);
            this.Name = "Notas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notas";
            this.MenuControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3P3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2P3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1P3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MenuControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSiguienteP1;
        private System.Windows.Forms.Button btnSiguienteP2;
        private System.Windows.Forms.Button btnSiguienteP3;
        private System.Windows.Forms.NumericUpDown nudNota1P1;
        private System.Windows.Forms.NumericUpDown nudNota2P1;
        private System.Windows.Forms.NumericUpDown nudNota3P1;
        private System.Windows.Forms.NumericUpDown nudNota3P2;
        private System.Windows.Forms.NumericUpDown nudNota2P2;
        private System.Windows.Forms.NumericUpDown nudNota1P2;
        private System.Windows.Forms.NumericUpDown nudNota3P3;
        private System.Windows.Forms.NumericUpDown nudNota2P3;
        private System.Windows.Forms.NumericUpDown nudNota1P3;
    }
}