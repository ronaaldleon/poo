﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia5
{
    public partial class MenuEjercicios : Form
    {
        public MenuEjercicios()
        {
            InitializeComponent();
        }

        private void btnEjercicio1_Click(object sender, EventArgs e)
        {
            //Invoco el form del ejemplo 2 el cual se tomo como el ejercicio 1 por sus modificaciones
            G5_Ejemplo_02 G5_Ejercicio_01 = new G5_Ejemplo_02();
            G5_Ejercicio_01.Show();
        }

        private void btnEjercicio2_Click(object sender, EventArgs e)
        {
            //Invoco el form del ejercicio 2
            Ingreso G5_Ejercicio_02 = new Ingreso();
            G5_Ejercicio_02.Show();
        }
    }
}
