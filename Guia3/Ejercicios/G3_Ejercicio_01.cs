﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicios
{
    public partial class G3_Ejercicio_01 : Form
    {
        public G3_Ejercicio_01()
        {
            InitializeComponent();
            //Invoco función que llena los comboBox e inicio el formulario con un tamaño personalizado
            Reinicio();
            Size = new Size(325, 437);
        }

        //instancia de la clase Cliente
        Cliente miCliente = new Cliente();

        //Función de llenado de comboBoxes
        private void Reinicio()
        {
            //Llenado de comboBox para el tipo de cuenta
            cboTipoCuenta.Items.Add("Cuenta corriente");
            cboTipoCuenta.Items.Add("Cuenta de ahorros");
            cboTipoCuenta.Items.Add("Cuenta a plazos");

            //Llenado de comboBox para las sucursales
            cboSucursal.Items.Add("Soyapango");
            cboSucursal.Items.Add("San Salvador");
            cboSucursal.Items.Add("San Miguel");
            cboSucursal.Items.Add("Santa Ana");
            cboSucursal.Items.Add("Chalatenango");
        }

        /*Listado que permite tener varios elementos de la clase Persona*/
        private List<Cliente> Clientes = new List<Cliente>();
        //el indice para editar comienza en -1, esto significa que no hay ningun seleccionado, esto servirá para el DataGridView
        private int contador = -1;

        private void actualizarGrid()
        {
            dgvClientes.DataSource = null;
            //Los nombre de columna de veremos son los de las propiedad
            dgvClientes.DataSource = Clientes;
        }

        private void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDui.Clear();
            txtNit.Clear();
            cboTipoCuenta.Items.Clear();
            txtMonto.Clear();
            cboSucursal.Items.Clear();
            //Luego de cada limpiado de campos tambien vuelvo a llenar los comboBoxes
            Reinicio();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" || txtApellido.Text != "" || txtDui.Text != "" || txtNit.Text != "" || txtMonto.Text != "")
            {
                //Expresión regular del formato del DUI
                Regex regexDui = new Regex(@"\d{8}-\d$");
                //Si lo escrito en el campo coincide con el formato continua...
                if (regexDui.IsMatch(txtDui.Text))
                {
                    //Expresión regular del formato del NIT
                    Regex regexNit = new Regex(@"\d{4}-\d{6}-\d{3}-\d{1}$");
                    //Si lo escrito en el campo coincide con el formato continua...
                    if (regexNit.IsMatch(txtNit.Text))
                    {
                        //cadena de expresiones regulares donde solo se aceptan números
                        Regex regexNumero = new Regex(@"^[0-9]+$");
                        //Si lo escrito en el campo son números sigue...
                        if (regexNumero.IsMatch(txtMonto.Text))
                        {
                            //Valida que ya haya seleccionado una opción del comboBox
                            if (cboTipoCuenta.SelectedIndex != -1)
                            {
                                if (cboSucursal.SelectedIndex != -1)
                                {
                                    //Calculo la cantidad de filas dentro del DGV para aumentarlo según el correlativo de cada cuenta creada
                                    int cant = dgvClientes.Rows.Count;

                                    //creo un objeto de la clase cliente y guardo a través de las propiedades
                                    Cliente cli = new Cliente();
                                    //Dentro del campo del número de cuenta en el DGV muestro el número de cuenta extraido del metodo que me crea una parte del correlativo según el tipo de cuenta
                                    cli.NumeroCuenta = Convert.ToString(miCliente.generaNumeroCuenta(cboTipoCuenta.SelectedItem.ToString())) + (cant + 1).ToString();
                                    cli.Nombre = txtNombre.Text;
                                    cli.Apellido = txtApellido.Text;
                                    cli.Dui = txtDui.Text;
                                    cli.Nit = txtNit.Text;
                                    cli.TipoCuenta = cboTipoCuenta.SelectedItem.ToString();
                                    cli.Monto = txtMonto.Text;
                                    cli.Sucursal = cboSucursal.SelectedItem.ToString();

                                    if (contador > -1) //verifica si hay un índice seleccionado
                                    {
                                        Clientes[contador] = cli;
                                        contador = -1;
                                    }
                                    else
                                    {
                                        //Al arreglo de Personas le agrego el objeto creado con todos los datos que recolecté
                                        Clientes.Add(cli);
                                    }

                                    //llamamos al procedimiento que guarda en datagrid
                                    actualizarGrid();
                                    //mandamos a llamar la función que limpia
                                    limpiar();
                                    //habilitamos boton para ver a los clientes
                                    btnMostrar.Visible = true;

                                    MessageBox.Show("¡Cuenta creada con exito!");
                                }
                                else
                                {
                                    MessageBox.Show("Seleccione la sucursal de apertura");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Seleccione el tipo de cuenta a aperturar");
                            }
                        }
                        else
                        {
                            MessageBox.Show("¡Solo se aceptan números en el monto!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡El formato de números del NIT es: ####-######-###-#!");
                    }
                }
                else
                {
                    MessageBox.Show("¡El formato de números del DUI es: ########-#!");
                }            
            }
            else
            {
                MessageBox.Show("¡Rellene los campos vacíos!");
            }            
        }

        private void dgvClientes_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dgvClientes.SelectedRows[0];
            int pos = dgvClientes.Rows.IndexOf(seleccion);//almacena en cual fila estoy
            contador = pos;//copio esa variable en indice editado

            Cliente cli = Clientes[pos];// esta variable de tipo cliente, se carga con los valores que les pasa el listado

            //lo que tiene el atributo se lo doy al textBox
            txtNombre.Text = cli.Nombre; 
            txtApellido.Text = cli.Apellido;
            txtDui.Text = cli.Dui;
            txtNit.Text = cli.Nit;
            cboTipoCuenta.SelectedItem = cli.TipoCuenta;
            txtMonto.Text = cli.Monto;
            cboSucursal.SelectedItem = cli.Sucursal;
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            //Cambio tamaño del formulario para poder ver el DataGridView
            Size = new Size(740, 437);
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            //Oculto nuevamente el boton que me posibilita mostar la lista de clientes
            btnMostrar.Visible = false;
            //Reinicio el tamaño del formulario según el tamaño de inicio original
            Size = new Size(325, 437);
        }
    }
}
