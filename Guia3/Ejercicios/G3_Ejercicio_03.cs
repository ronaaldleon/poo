﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicios
{
    public partial class G3_Ejercicio_03 : Form
    {
        public G3_Ejercicio_03()
        {
            InitializeComponent();
        }

        /*listado que permite tener varios elementos de la clase Persona*/
        private List<Producto> Productos = new List<Producto>();
        private int edit_indice = -1; //el índice para editar comienza en -1, esto significa que no hay ninguno seleccionado, esto servirá para el DataGridView.

        //Clase ya definida para el manejo de imagen
        OpenFileDialog imagenControl = new OpenFileDialog();

        private void actualizarGrid()
        {
            dgvListado.DataSource = null;
            /*los nombres de columna que veremos son los de las propiedades*/
            dgvListado.DataSource = Productos;
        }

        private void reseteo()
        {
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtMarca.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
            //Dejo de mostrar la imagen en el pictureBox nada más
            pictureBoxImagen.Image = null;
        }

        private void dgvListado_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow selected = dgvListado.SelectedRows[0];
            int posicion = dgvListado.Rows.IndexOf(selected);//almacena en cual fila estoy
            edit_indice = posicion; //copia esa variable en indice editado

            //Esta variable de tipo persona, se carga con los valores que le pasa el listado
            Producto product = Productos[posicion];

            //lo que tiene el atributo se lo doy al textbox
            txtNombre.Text = product.Nombre;
            txtDescripcion.Text = product.Descripcion;
            txtMarca.Text = product.Marca;
            txtPrecio.Text = Convert.ToString(product.Precio);
            txtStock.Text = Convert.ToString(product.Stock);
            //guardo la ruta de la imagen como string en la propiedad
            pictureBoxImagen.Image = Image.FromFile(product.Imagen);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Validación de campos vacíos
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && !string.IsNullOrEmpty(txtMarca.Text) && !string.IsNullOrEmpty(txtPrecio.Text) && !string.IsNullOrEmpty(txtStock.Text))
            {
                //cadena de expresiones regulares donde solo se aceptan números positivos
                Regex regexNumero = new Regex(@"^[0.0-9.9]+$");
                //Sí los campos de stock y precio son números sigue...
                if (regexNumero.IsMatch(txtPrecio.Text) && regexNumero.IsMatch(txtStock.Text))
                {
                    //Valido que hayan seleccionado una imagen para el producto
                    if (pictureBoxImagen.Image != null)
                    {
                        //Creo un Objeto de la clase persona y guardo a través de las propiedades
                        Producto product = new Producto();
                        product.Nombre = txtNombre.Text;
                        product.Descripcion = txtDescripcion.Text;
                        product.Marca = txtMarca.Text;
                        product.Precio = float.Parse(txtPrecio.Text);
                        product.Stock = int.Parse(txtStock.Text);
                        //Guardo la ruta de la imagen en la propiedad del objeto
                        product.Imagen = imagenControl.FileName;

                        if (edit_indice > -1) //verifica si hay un indice seleccionado
                        {
                            Productos[edit_indice] = product;
                            edit_indice = -1;
                        }
                        else
                        {
                            //Al arreglo de productos le agrego el objeto creado con todos los datos que recolecté
                            Productos.Add(product);
                        }

                        actualizarGrid();//llamamos al procedimiento que guarda en datagrid
                        reseteo();//llamamos al metodo que resetea
                    }
                    else
                    {
                        MessageBox.Show("¡Seleccione una imagen del producto!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("¡El precio y stock deben ser en números!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("¡Rellene el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }                        
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Productos.RemoveAt(edit_indice);
                edit_indice = -1; //resetea variable a -1
                reseteo();
                actualizarGrid();
            }
            else
            {
                MessageBox.Show("Dar doble click sobre elemento para seleccionar y borrar ");
            }
        }

        
        private void btnAggFoto_Click(object sender, EventArgs e)
        {
            //Filtro para cuando mande a elegir una imagen, el archivo sea del tipo imagenes
            imagenControl.Filter = "Archivo de imagenes (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";

            //Sí en el cuadro de selección de imagen le doy abrir u OK hago mostrar la imagen en el pictureBox
            if (imagenControl.ShowDialog() == DialogResult.OK)
            {                
                pictureBoxImagen.Image = Image.FromFile(imagenControl.FileName);               
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //Solo vació los campos de datos del producto en caso se le de doble click en DataGridView para ver el producto o cancelar un cambio
            reseteo();
        }
    }
}
