﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia3
{
    public partial class AgendaRecibe : Form
    {
        public List<Persona> PersonaRecibe = null; //creación de una lista que reciba

        public AgendaRecibe()
        {
            InitializeComponent();
        }

        private void actualizarGrid() //función que llena el DGV del formulario 2
        {
            dgvLlenado.DataSource = null;
            dgvLlenado.DataSource = PersonaRecibe;
        }

        private void btnLlenar_Click_1(object sender, EventArgs e)
        {
            actualizarGrid();
            //actualiza DGV cada vez que se presione.
        }
    }
}

