﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia3
{
    public partial class Inventario : Form
    {
        public Inventario()
        {
            InitializeComponent();
        }


        /*listado que permite tener varios elementos de la clase Persona*/
        private List<Producto> Productos = new List<Producto>();
        private int edit_indice = -1; //el índice para editar comienza en -1, esto significa que no hay ninguno seleccionado, esto servirá para el DataGridView.

        private void actualizarGrid()
        {
            dgvListado.DataSource = null;
            /*los nombres de columna que veremos son los de las propiedades*/
            dgvListado.DataSource = Productos;             
        }

        private void reseteo()
        {
            txtNombre.Clear();
            txtDescripcion.Clear();
            txtMarca.Clear();
            txtPrecio.Clear();
            txtStock.Clear();
        }

        private void dgvListado_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow selected = dgvListado.SelectedRows[0];
            int posicion = dgvListado.Rows.IndexOf(selected);//almacena en cual fila estoy
            edit_indice = posicion; //copia esa variable en indice editado

            //Esta variable de tipo persona, se carga con los valores que le pasa el listado
            Producto product = Productos[posicion];

            //lo que tiene el atributo se lo doy al textbox
            txtNombre.Text = product.Nombre;
            txtDescripcion.Text = product.Descripcion;
            txtMarca.Text = product.Marca;
            txtPrecio.Text = Convert.ToString(product.Precio);
            txtStock.Text = Convert.ToString(product.Stock);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Creo un ibjeto de la clase persona y guardo a tra vés de las propiedades
            Producto product = new Producto();
            product.Nombre = txtNombre.Text;
            product.Descripcion = txtDescripcion.Text;
            product.Marca = txtMarca.Text;
            product.Precio = float.Parse(txtPrecio.Text);
            product.Stock = int.Parse(txtStock.Text);

            if(edit_indice > -1) //verifica si hay un indice seleccionado
            {
                Productos[edit_indice] = product;
                edit_indice = -1;
            }
            else
            {
                //Al arreglo de productos le agrego el objeto creado con todos los datos que recolecté
                Productos.Add(product);
            }

            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            reseteo();//llamamos al metodo que resetea
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Productos.RemoveAt(edit_indice);
                edit_indice = -1; //resetea variable a -1
                reseteo();
                actualizarGrid();
            }
            else
            {
                MessageBox.Show("Dar doble click sobre elemento para seleccionar y borrar ");
            }

        }
    }
}
