﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia3
{
    public partial class AgendaEnvia : Form
    {
        public AgendaEnvia()
        {
            InitializeComponent();
        }

        /*Listado que permite tener varios elementos de la clase Persona*/
        private List<Persona> Personas = new List<Persona>();
        //el indice para editar comienza en -1, esto significa que no hay ningun seleccionado, esto servirá para el DataGridView
        private int edit_indice = -1;


        private void actualizarGrid()
        {
            DgvContactos.DataSource = null;
            //Los nombre de columna de veremos son los de las propiedad
            DgvContactos.DataSource = Personas;
        }

        private void limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtTelefono.Clear();
            txtCorreo.Clear();
        }

        private void DgvContactos_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = DgvContactos.SelectedRows[0];
            int pos = DgvContactos.Rows.IndexOf(seleccion);//almacena en cual fila estoy
            edit_indice = pos;//copio esa variable en indice editado

            Persona per = Personas[pos];// esta variable de tipo persona, se carga con los valores que les pasa el listado

            txtNombre.Text = per.Nombre; //lo que tiene el atributo se lo doy al textBox
            txtApellido.Text = per.Apellido;
            txtTelefono.Text = per.Telefono;
            txtCorreo.Text = per.Correo;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //creo un obnjeto de la clase persona y guardo a través de las propiedades
            Persona per = new Persona();
            per.Nombre = txtNombre.Text;
            per.Apellido = txtApellido.Text;
            per.Telefono = txtTelefono.Text;
            per.Correo = txtCorreo.Text;

            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Personas[edit_indice] = per;
                edit_indice = -1;
            }
            else
            {
                //Al arreglo de Personas le agrego el objeto creado con todos los datos que recolecté
                Personas.Add(per);
            }

            //llamamos al procedimiento que guarda en datagrid
            actualizarGrid();
            //mandamos a llamar la función que limpia
            limpiar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (edit_indice > -1) //verifica si hay un indice seleccionado
            {
                Personas.RemoveAt(edit_indice);
                edit_indice = -1; //resetea variable a -1
                limpiar();
                actualizarGrid();
            }
            else
            {
                MessageBox.Show("Debe dar doble click primero sobre contacto");
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            AgendaRecibe formulario = new AgendaRecibe(); //instancia de otro formulario

            /*lista original Personas es enviada a la lista PersonaRecibe que está en el 
             * AgendaRecibe y que puede ser invocada por medio de la instancia del segundo formulario */
            formulario.PersonaRecibe = Personas; 

            formulario.Show(); //mostar el segundo formulario
        }
    }
}
