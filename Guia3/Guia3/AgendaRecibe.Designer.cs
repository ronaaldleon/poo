﻿
namespace Guia3
{
    partial class AgendaRecibe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvLlenado = new System.Windows.Forms.DataGridView();
            this.btnLlenar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLlenado)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLlenado
            // 
            this.dgvLlenado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLlenado.Location = new System.Drawing.Point(12, 12);
            this.dgvLlenado.Name = "dgvLlenado";
            this.dgvLlenado.RowHeadersWidth = 51;
            this.dgvLlenado.RowTemplate.Height = 24;
            this.dgvLlenado.Size = new System.Drawing.Size(625, 416);
            this.dgvLlenado.TabIndex = 0;
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(284, 465);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(80, 30);
            this.btnLlenar.TabIndex = 1;
            this.btnLlenar.Text = "LLenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click_1);
            // 
            // AgendaRecibe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 524);
            this.Controls.Add(this.btnLlenar);
            this.Controls.Add(this.dgvLlenado);
            this.Name = "AgendaRecibe";
            this.Text = "AgendaRecibe";
            ((System.ComponentModel.ISupportInitialize)(this.dgvLlenado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLlenado;
        private System.Windows.Forms.Button btnLlenar;
    }
}